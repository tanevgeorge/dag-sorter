### DAG Sorter

This is a small desktop application based on the Java Swing framework, enabling interactive construction and 
visualization of [directed acyclic graphs](https://en.wikipedia.org/wiki/Directed_acyclic_graph) and 
their [topological orderings](https://en.wikipedia.org/wiki/Topological_sorting).

To run the application, clone or download this repository and execute the `dist/DAG Sorter.jar` file locally.
Make sure you have a working JRE or JDK (v1.6 or higher) installed on your machine.